# Summary #

The project is designed with four layers.  Core, Data, DataProcess(business logic), KPMGTest.

* Core layer: defined all the public interface and entities.
* Data: implemented DAL and CSV reader.
* DataProcess: implemented business logic.
* KPMGTest: asp.net MVC structure. 

### Database ###

It designed with Entity Framework code first. It leverages DAL generic Repository and unitOfWork designing pattern.  I used the localdb as persistence database, which can be set as any other kind, which is set in web.config. In the asp.net MVC, I use Ninject to inject the UOW into controller, which can befit the SOC and Unit of test.

To increase the performance, I utilized the SqlBulkCopy to persistent the validated data into table, which seems very efficient.  


### File ###

* File is supported by XLSX and CSV only. 
* The files are uploaded in the fold “ ~/App_Data/uploads”.

### UI ###

As time limited, I just did the basic function of the requirement. The better practise should be fulfilled with JQuery to make asynchronous request and response. 

Thank you!