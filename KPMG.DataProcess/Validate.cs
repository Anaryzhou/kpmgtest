﻿using KPMG.Core;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPMG.DataProcess
{
    public class Validate : IValidate
    {
        private readonly List<string> currencyCodes;

        public Validate()
        {            
            currencyCodes = Utils.CurrencyCodes;
        }

        //It can improve the performance significantly, comparing with LINQ
        private bool containsLoop(List<string> list, string value)
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i] == value)
                {
                    return true;
                }
            }
            return false;
        }

        public bool Valiate(OrgData data)
        {
            decimal number;            
            var result = false;
            if (data != null)
            {
                var canConvert = decimal.TryParse(data.Amount, out number);
                var isCurrencyCode = containsLoop(currencyCodes, data.CurrencyCode);

                result = (!string.IsNullOrEmpty(data.Account)) &&
                         (!string.IsNullOrEmpty(data.Description)) &&
                         (!string.IsNullOrEmpty(data.CurrencyCode)) && canConvert && isCurrencyCode;
            }
            return result;
        }     
    }
}
