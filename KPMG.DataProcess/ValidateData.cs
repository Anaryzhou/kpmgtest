﻿using KPMG.Core;
using KPMG.Core.Contract;
using KPMG.Core.Entities;
using System;
using System.CodeDom;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KPMG.Data;
using KPMG.Data.DAL;

namespace KPMG.DataProcess
{
    
    public class ValiateData : IValidateData
    {
        IValidate validator;

        public ValiateData()
        {
            this.validator = new Validate();
        }

        public ValiateData(IValidate validate, IProcessMessage processor)
        {
            this.validator = validate;            
        }

        public ValidatedTransactions ValidatedData(IEnumerable<OrgData> dataList)
        {
            var result = new ValidatedTransactions();
            var failed = new List<OrgData>();
            var successed = new List<Transaction>();
            var counter = 0;
            
           
                foreach (var data in dataList)
                {
                    if (validator.Valiate(data))
                    {
                        counter++;
                        successed.Add(TransConverter(data));
                    }
                    else
                    {

                        failed.Add(data);                       
                    }
                }
            var dataTable = successed.ToDataTable();
            var csDestination = ConfigurationManager.ConnectionStrings["TransactionContext"].ConnectionString;
            Database.CopyToTable(csDestination,"Transactions",dataTable);
            
            return new ValidatedTransactions()
            {
                FailedTransactions = failed,
                TotalCount=counter
            };
        }

        private Transaction TransConverter(OrgData data)
        {
            return new Transaction()
            {
                Account = data.Account,
                Description = data.Description,
                CurrencyCode = data.CurrencyCode,
                Amount = decimal.Parse(data.Amount)
            };

        }
    }

  
}
