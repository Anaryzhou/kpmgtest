﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using KPMG.Data.DAL;
using KPMG.DataProcess;

namespace ConsoleTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var path = @"C:\Users\Andy\Documents\t1.xlsx";
            var watch = Stopwatch.StartNew();
            
            
            Database.CleanTable();
            
            var data = new ReadInData();
            var ts = data.GetData(path);

            
            ValiateData valiator = new ValiateData();
            var lists = valiator.ValidatedData(ts);
            watch.Stop();
            Console.WriteLine(watch.Elapsed.TotalSeconds);
            Console.WriteLine("Total process records: {0}",lists.TotalCount);
            Console.WriteLine(lists.FailedTransactions.Count);
            Console.ReadKey();

        }
    }
}
