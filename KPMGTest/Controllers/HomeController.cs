﻿using KPMG.Core.Contract;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KPMG.Data.DAL;
using KPMG.DataProcess;
using KPMGTest.Models;

namespace KPMGTest.Controllers
{
    public class HomeController : Controller
    {
        private IGetData data;
        private IValidateData invalidator;
       

        public HomeController(IGetData data,
                                  IValidateData invalidator)
        {
            this.data = data;
            this.invalidator = invalidator;
            
        }
        public ActionResult Index()
        {
            var model = new UploadViewModel();

            model.IsFirstRowAsColumnName = true;
            return View(model);
        } 

        [HttpPost]
        public ActionResult Index(UploadViewModel model)
        {
            if (ModelState.IsValid)
            {
                var fileName = Path.GetFileName(model.File.FileName);
                if (fileName.ToUpper().EndsWith(".XLSX") || fileName.ToUpper().EndsWith(".CSV"))
                {
                    if (model.File.ContentLength > 0)
                    {
                        Database.CleanTable();
                        var path = Path.Combine(Server.MapPath("~/App_Data/uploads"), fileName);
                        model.File.SaveAs(path);

                        var ts = data.GetData(path, model.IsFirstRowAsColumnName);
                        var lists = invalidator.ValidatedData(ts);
                        Session["Errors"] = lists;
                        Session["Count"] = ts.Count();
                        return RedirectToAction("index", "Transaction");
                    }
                    return View("index");
                }
            }
            return View("index");
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
