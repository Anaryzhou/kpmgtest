﻿using KPMG.Core.Contract;
using KPMG.Core.Entities;
using System.Data.Entity;


namespace KPMG.Data.DAL.Entity
{
    public class KPMGTransactionContext : DbContext, IDbContext
    {
        public KPMGTransactionContext() :base("TransactionContext")
        {
            System.Data.Entity.Database.SetInitializer(new CreateDatabaseIfNotExists<KPMGTransactionContext>());
        }

        public new IDbSet<T> Set<T>() where T : class
        {
            return base.Set<T>();
        }

        public DbSet<Transaction> Transactions { get; set; }
    }
}
