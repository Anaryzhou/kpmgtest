﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using KPMG.Core.Entities;
using KPMG.Data.DAL.Entity;

namespace KPMG.Data.DAL
{
    public static class Database
    {
        public static void CopyToTable(string csDestionation, string tableName, DataTable data)
        {
            using (SqlConnection destinationConnection = new SqlConnection(csDestionation))
            {
                destinationConnection.Open();
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(destinationConnection))
                {
                    bulkCopy.DestinationTableName = tableName;
                    bulkCopy.WriteToServer(data);
                }
            }
        }

        public static void CleanTable()
        {
            using (var dbContext = new KPMGTransactionContext())
            {
                dbContext.Database.ExecuteSqlCommand("truncate table Transactions");
            }
        }
    }
}