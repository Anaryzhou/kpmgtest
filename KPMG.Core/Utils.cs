﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using KPMG.Core.Entities;

namespace KPMG.Core
{
    public static class Utils
    {
        //Convert Data to DataTable
        public static DataTable ToDataTable<T>(this IList<T> data)
        {
            PropertyDescriptorCollection props =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                table.Columns.Add(prop.Name, prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }


        public static readonly List<string> CurrencyCodes=
            CultureInfo
                .GetCultures(CultureTypes.AllCultures)

                .Select(culture =>
                {
                    try
                    {

                        return new RegionInfo(culture.Name).ISOCurrencySymbol;
                    }
                    catch
                    {
                        return null;
                    }
                }).Where(ri => ri != null).Distinct().ToList();

    }  
}
