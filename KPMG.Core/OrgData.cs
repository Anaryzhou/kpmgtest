﻿namespace KPMG.Core
{
    public class OrgData
    {
        public string Account { get; set; }
        public string Amount { get; set; }
        public string CurrencyCode { get; set; }
        public string Description { get; set; }
    }
}
